# Advent of Code 2020

My solutions to this year's calendar from [adventofcode.com](https://adventofcode.com/2020).
I'm going to try writing all of them in Rust, just for the fun of it.

I'll be using the `cargo-aoc` for it's benchmarking and fetching of input files.

## Day 1: Report Repair
Re-did it using a hashset, which was significantly faster.
### Part 1
Found `996996` in `4.2874 µs`.
### Part 2
Found `9210402` in `33.043 µs`.

## Day 2: Password Philosophy
Tried multi-threading today, it's not pretty, but it runs at 200% CPU.
### Part 1
Found `418` in `16.978 ms`.
### Part 2
Found `616` in `17.029 ms`.

## Day 3: Toboggan Trajectory
Nothing special
### Part 1
Found `203` in `2.9597 µs`.
### Part 2
Found `3316272960` in `13.327 µs`.

## Day 4: Passport Processing
Part 2 turned out to be quite heavy RegEx
### Part 1
Found `245` in `225.43 µs`.
### Part 2
Found `133` in `2.2929 ms`.

## Day 5: Binary Boarding
I had to reread part 2 multiple times, could have been more clear.
### Part 1
Found `838` in `52.575 µs`.
### Part 2
Found `714` in `76.360 µs`.

## Day 6: Custom Customs
Relatively simple with the use of an HashSet and HashMap
### Part 1
Found `6868` in `222.23 µs`.
### Part 2
Found `3550` in `277.14 µs`.

## Day 7: Handy Haversacks
The most difficult part was all the input parsing
### Part 1
Found `121` in `594.41 µs`.
### Part 2
Found `3805` in `431.72 µs`.

## Day 8: Handheld Halting
A simple and quick day, nice
### Part 1
Found `1766` in `11.475 µs`.
### Part 2
Found `1639` in `561.63 µs`.

## Day 9: Encoding Error
Time for queues!
### Part 1
Found `36845998` in `15.138 µs`.
### Part 2
Found `4830226` in `16.985 µs`.
