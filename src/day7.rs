use std::collections::{HashMap, HashSet};
use regex::Regex;

///# Day 7: Handy Haversacks
/// https://adventofcode.com/2020/day/7
#[aoc_generator(day7)]
pub fn prepare_input(input: &str) -> Vec<String> {
    input.lines().map(|s| s.trim().to_string()).collect()
}


///## How many bag colors can eventually contain at least one shiny gold bag?
/// `121`
#[aoc(day7, part1)]
pub fn solve_part1(input: &Vec<String>) -> Option<usize> {
    // Parse lines into a hashmap of each bag and it's contents
    let mut bags_with_contents = HashMap::new();
    input.iter().map(|line| parse_bag(line)).for_each(|(bag, contents)| {
        bags_with_contents.insert(bag, contents);
    });

    // Make the target
    let target_bag = "shiny gold".to_string();
    // Use a "cache" to speed things up, no need to check things twice
    let mut bag_cache = (HashSet::new(), HashSet::new());
    // For each bag
    for (bag, _) in &bags_with_contents {
        // Is the bag not in a cache, we haven't visited it yet
        if !bag_cache.0.contains(bag) && !bag_cache.1.contains(bag) {
            // So let's check it, and add it to one of the caches
            if solve_bag_contains_target(&target_bag, &mut bag_cache, &bags_with_contents, bag) {
                // Contains target
                bag_cache.0.insert(bag.clone());
            } else {
                // Doesn't contain target
                bag_cache.1.insert(bag.clone());
            }
        }
    }

    // How many are in the "contains target" cache?
    Some(bag_cache.0.len())
}


///## How many individual bags are required inside your single shiny gold bag?
/// `3805`
#[aoc(day7, part2)]
pub fn solve_part2(input: &Vec<String>) -> Option<usize> {
    // Parse lines into a hashmap of each bag and it's contents
    let mut bags_with_contents = HashMap::new();
    input.iter().map(|line| parse_bag(line)).for_each(|(bag, contents)| {
        bags_with_contents.insert(bag, contents);
    });

    // Make the target
    let target_bag = "shiny gold".to_string();
    // Recursively check how many bags fit in the golden bag, and subtract itself
    let bags_in_bag = solve_bags_in_bag(&bags_with_contents, &target_bag) - 1;

    Some(bags_in_bag)
}

// Some types to simplify some of the logic
type BagCache = (HashSet<String>, HashSet<String>);
type Contents = Vec<(usize, String)>;
type BagWithContents = (String, Contents);
type BagsWithContents = HashMap<String, Contents>;


// Parse each line into a bag with contents
fn parse_bag(line: &str) -> BagWithContents {
    lazy_static! {
        static ref CONTENTS_SPLIT_REGEX: Regex = Regex::new(r" bags?[.,] ?").unwrap();
    }
    // Split line, anything before the split is the bag, anything after is the contents
    let mut line = line.split(" bags contain ");
    let bag = line.next().unwrap().to_string();
    let contents = line.next().unwrap();

    // Split the contents and return as a tuple
    let contents = CONTENTS_SPLIT_REGEX.split(contents)
        // Filter out any empty lines
        .filter(|content| !content.is_empty())
        .map(|content| {
            let mut content = content.splitn(2, " "); // "12 shiny purple"
            (
                content.next().unwrap().parse::<usize>().unwrap_or_default(), // "12"
                content.next().unwrap().to_string() // "shiny purple"
            )
        })
        // Filter out any 0, as these contain no bags
        .filter(|x| x.0 != usize::default())
        .collect();

    (bag, contents)
}


// Recursively solve in how many bags the target bag goes
fn solve_bag_contains_target(
    target_bag: &String,
    bag_cache: &mut BagCache,
    bags_with_contents: &BagsWithContents,
    bag: &String,
) -> bool {
    // Grab contents of current bag
    let contents = &bags_with_contents[bag];
    // For each of it's content
    for (_, sub_bag) in contents {
        // If the bag is in the "doesn't contain" cache, continue to next iteration
        if bag_cache.1.contains(sub_bag) { continue; }
        // The content bag is the target bag,
        if sub_bag == target_bag ||
            // or we know it contains the target bag,
            bag_cache.0.contains(sub_bag) ||
            // or it checks out to contain the target bag
            solve_bag_contains_target(target_bag, bag_cache, bags_with_contents, sub_bag) {
            // Add the current bag to the "contains" cache
            bag_cache.0.insert(bag.clone());
            // And stop, we don't need to check further
            return true;
        }
    }
    // None of the content bags contained the target, so mark this bag as such
    bag_cache.1.insert(bag.clone());
    false
}


// Recursively solve how many bags must be in a bag
fn solve_bags_in_bag(bags_with_contents: &BagsWithContents, bag: &String) -> usize {
    // Grab contents of current bag
    let contents = &bags_with_contents[bag];
    let mut result = 1;

    // Loop through the contents and multiply the recursive result
    for (count, sub_bag) in contents {
        result += solve_bags_in_bag(bags_with_contents, sub_bag) * count;
    }

    result
}
