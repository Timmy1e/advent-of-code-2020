const CHAR_TREE: char = '#';

///# Day 3: Toboggan Trajectory
/// https://adventofcode.com/2020/day/3
#[aoc_generator(day3)]
pub fn prepare_input(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|s| s.trim().chars().collect()).collect()
}


///## Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many trees would you encounter?
#[aoc(day3, part1)]
pub fn solve_part1(input: &Vec<Vec<char>>) -> Option<usize> {
    Some(solve_slope(input, (3, 1)))
}


///## What do you get if you multiply together the number of trees encountered on each of the listed slopes?
#[aoc(day3, part2)]
pub fn solve_part2(input: &Vec<Vec<char>>) -> Option<usize> {
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    let mut result = 1; // Result should be 1 as we are multiplying
    for slope in slopes {
        result *= solve_slope(input, slope);
    }
    Some(result)
}


fn solve_slope(input: &Vec<Vec<char>>, slope: (usize, usize)) -> usize {
    let mut pos = (0, 0);
    let mut result = 0;

    while pos.1 < input.len() - 1 {
        // Add slope to position, wrap X
        pos.0 = (pos.0 + slope.0) % input[pos.0].len();
        pos.1 += slope.1;
        // Check is current position is a tree
        if input[pos.1][pos.0] == CHAR_TREE {
            result += 1;
        }
    }

    result
}
