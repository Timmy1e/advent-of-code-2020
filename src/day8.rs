use std::collections::HashSet;


///# Day 8: Handheld Halting
/// https://adventofcode.com/2020/day/8
#[aoc_generator(day8)]
pub fn prepare_input(input: &str) -> Vec<Instruction> {
    input.lines().map(|s| {
        let mut split = s.trim().split(' ');
        // Parse the lines as instructions
        match split.next().unwrap() {
            "acc" => Instruction::ACC(split.next().unwrap().parse().unwrap()),
            "jmp" => Instruction::JMP(split.next().unwrap().parse().unwrap()),
            // Treat any other line as a NOP
            _ => Instruction::NOP(split.next().unwrap().parse().unwrap())
        }
    }).collect()
}


///## Run your copy of the boot code. Immediately before any instruction is executed a second time, what value is in the accumulator?
/// `1766`
#[aoc(day8, part1)]
pub fn solve_part1(input: &Vec<Instruction>) -> Option<isize> {
    // Set some "VM" registers
    let mut accumulator = 0;
    let mut program_counter = 0;
    // Keep track of the visited indexes for loops
    let mut visited_indexes = HashSet::new();

    loop {
        // If we are visiting an index we have already visited we are looping, so stop
        if visited_indexes.contains(&program_counter) {
            return Some(accumulator);
        } else {
            // Otherwise add the current address to the visited indexes
            visited_indexes.insert(program_counter);
        }
        match input[program_counter as usize] {
            Instruction::ACC(x) => {
                accumulator += x;
                program_counter += 1;
            }
            Instruction::JMP(y) => { program_counter += y }
            Instruction::NOP(_) => { program_counter += 1 }
        }
    }
}


///## What is the value of the accumulator after the program terminates?
/// `1639`
#[aoc(day8, part2)]
pub fn solve_part2(input: &Vec<Instruction>) -> Option<isize> {
    let input_size = (input.len() - 1) as isize;
    let mut changed_indexes = HashSet::new();

    'outer: loop {
        // Set some "VM" registers
        let mut accumulator = 0;
        let mut program_counter = 0;
        // Keep track of the visited indexes for loops
        let mut visited_indexes = HashSet::new();
        let mut have_changed_instruction = false;

        loop {
            if program_counter == input_size {
                // Success!
                return Some(accumulator);
            } else if visited_indexes.contains(&program_counter) {
                // Failure!
                continue 'outer;
            } else {
                // Keep going!
                visited_indexes.insert(program_counter);
            }

            match input[program_counter as usize] {
                Instruction::ACC(x) => {
                    accumulator += x;
                    program_counter += 1;
                }
                Instruction::JMP(y) => {
                    if !have_changed_instruction && !changed_indexes.contains(&program_counter) {
                        have_changed_instruction = true;
                        changed_indexes.insert(program_counter);
                        program_counter += 1; // Act like a NOP
                        continue;
                    }
                    program_counter += y;
                }
                Instruction::NOP(z) => {
                    if z != 0 && !have_changed_instruction && !changed_indexes.contains(&program_counter) {
                        have_changed_instruction = true;
                        changed_indexes.insert(program_counter);
                        program_counter += z; // Act like a JMP
                        continue;
                    }
                    program_counter += 1;
                }
            }
        }
    }
}


// Simplify instruction parsing
pub enum Instruction {
    ACC(isize),
    JMP(isize),
    NOP(isize),
}
