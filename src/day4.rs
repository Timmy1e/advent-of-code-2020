use regex::RegexSet;

///# Day 4: Passport Processing
/// https://adventofcode.com/2020/day/4
#[aoc_generator(day4)]
pub fn prepare_input(input: &str) -> Vec<String> {
    input.lines().map(|s| s.trim().to_string()).collect()
}


///## In your batch file, how many passports are valid?
/// `245`
#[aoc(day4, part1)]
pub fn solve_part1(input: &Vec<String>) -> Option<usize> {
    const REQUIRED_FIELDS: [&str; 7] = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"];
    let mut lines = input.clone();
    let mut result = 0;

    'lines: for index in 0..lines.len() {
        // Fold multiple passport lines into one
        if fold_lines(&mut lines, index) == None { continue; }

        // Check if the line contains all the missing fields, else skip to next line
        for required_field in REQUIRED_FIELDS.iter() {
            if !lines[index].contains(required_field) {
                continue 'lines;
            }
        }

        result += 1;
    }

    Some(result)
}


///##  your batch file, how many passports are valid?
/// `133`
#[aoc(day4, part2)]
pub fn solve_part2(input: &Vec<String>) -> Option<usize> {
    lazy_static! {
        static ref REQUIREMENTS_SET: RegexSet = RegexSet::new(&[
            r".*byr:(19[2-9][0-9]|200[0-2]).*",
            r".*iyr:(201[0-9]|2020).*",
            r".*eyr:(202[0-9]|2030).*",
            r".*hgt:((1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in).*",
            r".*hcl:#([0-9a-f]{6}).*",
            r".*ecl:(amb|blu|brn|gry|grn|hzl|oth).*",
            r".*pid:(\d{9}( |$)).*",
        ]).unwrap();
    }
    let mut lines = input.clone();
    let mut result = 0;

    for index in 0..lines.len() {
        // Fold multiple passport lines into one
        if fold_lines(&mut lines, index) == None { continue; }

        // Check if the passport matches the requirements by checking the length of the matched regexes to the total regexes
        if REQUIREMENTS_SET.matches(&lines[index]).into_iter().collect::<Vec<usize>>().len() == REQUIREMENTS_SET.len() {
            result += 1;
        }
    }

    Some(result)
}

fn fold_lines(lines: &mut Vec<String>, index: usize) -> Option<()> {
    let line = &lines[index].clone();

    // Skip empty line
    if line.is_empty() {
        return None;
    }

    // Add current line to next if next isn't empty
    return if index + 1 < lines.len() && !lines[index + 1].is_empty() {
        lines[index + 1] += " ";
        lines[index + 1] += line;
        None
    } else {
        Some(())
    };
}
