use itertools::Itertools;
use std::collections::HashSet;

// The target is the current year
const TARGET_SUM: usize = 2020;

///# Day 1: Report Repair
/// https://adventofcode.com/2020/day/1
#[aoc_generator(day1)]
pub fn prepare_input(input: &str) -> Vec<usize> {
    input.lines().map(|s| s.trim().parse().unwrap()).collect()
}

///## Find **TWO** entries that sum to 2020; what do you get if you multiply them together?
#[aoc(day1, part1)]
pub fn solve_part1(input: &Vec<usize>) -> Option<usize> {
    let hashset: HashSet<usize> = input.iter().cloned().collect();
    input
        .iter()
        .filter_map(|x| match hashset.get(&(TARGET_SUM - x)) {
            Some(y) => Some(x * y),
            _ => None
        })
        .nth(0)
}

///## Find **THREE** entries that sum to 2020; what do you get if you multiply them together?
#[aoc(day1, part2)]
pub fn solve_part2(input: &Vec<usize>) -> Option<usize> {
    let hashset: HashSet<usize> = input.iter().cloned().collect();
    input
        .iter()
        .cartesian_product(input)
        .filter_map(|(x, y)| {
            match if (x + y) < TARGET_SUM {
                hashset.get(&(TARGET_SUM - (x + y)))
            } else {
                None
            } {
                Some(z) => Some(x * y * z),
                _ => None
            }
        })
        .nth(0)
}
