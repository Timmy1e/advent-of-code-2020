use itertools::Itertools;
use std::collections::HashSet;
use std::iter::FromIterator;

const MAX_ROWS: usize = 127;
const MAX_COLUMNS: usize = 7;

///# Day 5: Binary Boarding
/// https://adventofcode.com/2020/day/5
#[aoc_generator(day5)]
pub fn prepare_input(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|s| s.trim().chars().collect()).collect()
}


///## What is the highest seat ID on a boarding pass?
/// `838`
#[aoc(day5, part1)]
pub fn solve_part1(input: &Vec<Vec<char>>) -> Option<usize> {
    // Convert the "tickets" to IDs
    let ticket_ids = get_ticket_ids(input);
    // Get the last ID, and return a copy
    ticket_ids.last().copied()
}

///## What is the ID of your seat?
/// `714`
#[aoc(day5, part2)]
pub fn solve_part2(input: &Vec<Vec<char>>) -> Option<usize> {
    // Convert the "tickets" to IDs
    let ticket_ids = get_ticket_ids(input);
    // Grab a copy of the last ID, so the input can go out of scope sooner
    let last_id = ticket_ids.last().copied().unwrap();
    // Convert the Vec to a HashSet for easy lookups
    let ticket_set: HashSet<usize> = HashSet::from_iter(ticket_ids);

    // Loop through the possible ID's which start after the first column, and can't be higher then one before the last found ID
    for ticket_id in MAX_COLUMNS..last_id {
        // This ID is you ticket if it doesn't exist but both of it's neighbours do
        if !ticket_set.contains(&ticket_id) &&
            ticket_set.contains(&(ticket_id - 1)) &&
            ticket_set.contains(&(ticket_id + 1)) {
            return Some(ticket_id);
        }
    }

    // Could not find a single gap in the IDs
    None
}


fn get_ticket_ids(tickets: &Vec<Vec<char>>) -> Vec<usize> {
    // For each "ticket" get the ID, and sort them
    tickets.iter().map(get_ticket_id).sorted().collect()
}


fn get_ticket_id(ticket: &Vec<char>) -> usize {
    let mut rows = (0, MAX_ROWS);
    let mut columns = (0, MAX_COLUMNS);

    // Loop through the "instructions" of the ticket
    for chr in ticket {
        // Get the top or bottom of the appropriate range
        match chr {
            'F' => rows.1 = get_average(rows),
            'B' => rows.0 = get_average(rows) + 1,
            'L' => columns.1 = get_average(columns),
            'R' => columns.0 = get_average(columns) + 1,
            _ => panic!()
        }
    }

    // Ticket ID conversion
    (rows.0 * 8) + columns.0
}


fn get_average((low, high): (usize, usize)) -> usize {
    (low + high) / 2
}
