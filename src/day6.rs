use std::collections::{HashSet, HashMap};

///# Day 6: Custom Customs
/// https://adventofcode.com/2020/day/6
#[aoc_generator(day6)]
pub fn prepare_input(input: &str) -> Vec<Vec<char>> {
    let mut lines: Vec<Vec<char>> = input.lines().map(|s| s.trim().chars().collect()).collect();
    lines.push(vec![]); // Make sure it ends on an empty line
    lines
}


///## For each group, count the number of questions to which **anyone** answered "yes". What is the sum of those counts?
/// `6885`
#[aoc(day6, part1)]
pub fn solve_part1(input: &Vec<Vec<char>>) -> Option<usize> {
    let mut result = 0;
    // Keep track of anything anyone answered yes to
    let mut group_answers = HashSet::new();
    for line in input {
        // For each line add the chars to the answers, it doesn't matter if it already exists
        line.iter().for_each(|chr| { group_answers.insert(*chr); });
        // If we find an empty line it must be the end of a group
        if line.is_empty() {
            // Add the amount of unique answers to the result
            result += group_answers.len();
            // And clear the group answers
            group_answers.clear();
        }
    }
    // Return the result
    Some(result)
}


///## For each group, count the number of questions to which **everyone** answered "yes". What is the sum of those counts?
/// `3550`
#[aoc(day6, part2)]
pub fn solve_part2(input: &Vec<Vec<char>>) -> Option<usize> {
    let mut result = 0;
    // Keep track of the groups size and answers
    let mut group_size = 0;
    let mut group_answers: HashMap<char, usize> = HashMap::new();
    for line in input {
        // Switch on the line
        if !line.is_empty() {
            // If the line isn't empty, add one to the size, and count up for each char they answered
            group_size += 1;
            line.iter().for_each(|chr| {
                *(group_answers.entry(*chr).or_insert(0)) += 1;
            });
        } else {
            // If the line is empty, check for each char how many answered it
            for (_, group_answer) in &group_answers {
                // If it is the same as the group size then everyone must have answerd it
                if *group_answer == group_size {
                    result += 1;
                }
            }
            // Reset the group info
            group_size = 0;
            group_answers.clear();
        }
    }
    // Guess what, return the result
    Some(result)
}
