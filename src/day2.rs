use regex::{Regex, Captures};
use std::thread;
use std::str::FromStr;


///# Day 2: Password Philosophy
/// https://adventofcode.com/2020/day/2
#[aoc_generator(day2)]
pub fn prepare_input(input: &str) -> Vec<String> {
    input.lines().map(|s| s.trim().to_string()).collect()
}


///## How many passwords are valid according to their policies?
#[aoc(day2, part1)]
pub fn solve_part1(input: &Vec<String>) -> Option<usize> {
    // For each line spawn a new thread, and check if the count in within the policy
    let mut threads = vec![];
    for line in input {
        let line_clone = line.clone();
        threads.push(thread::spawn(move || {
            let password = Password::new(&line_clone);
            let count = password.str.matches(password.char).count();
            password.min <= count && count <= password.max
        }));
    }

    // Wait for all the threads to be done, and count up how many returned true
    let mut valid_count = 0;
    for thread in threads {
        let result = thread.join();
        if let Ok(true) = result {
            valid_count += 1;
        }
    }

    Some(valid_count)
}


///## How many passwords are valid according to the new interpretation of the policies?
#[aoc(day2, part2)]
pub fn solve_part2(input: &Vec<String>) -> Option<usize> {
    // For each line spawn a new thread, get the chars policy positions, and check if they are exclusive
    let mut threads = vec![];
    for line in input {
        let line_clone = line.clone();
        threads.push(thread::spawn(move || {
            let password = Password::new(&line_clone);
            let first_char = password.str.chars().nth(password.min - 1).unwrap();
            let last_char = password.str.chars().nth(password.max - 1).unwrap();
            (password.char == first_char) ^ (password.char == last_char) // XOR
        }));
    }

    // Wait for all the threads to be done, and count up how many returned true
    let mut valid_count = 0;
    for thread in threads {
        let result = thread.join();
        if let Ok(true) = result {
            valid_count += 1;
        }
    }

    Some(valid_count)
}


#[derive(Debug)]
struct Password<'a> {
    pub min: usize,
    pub max: usize,
    pub char: char,
    pub str: &'a str,
}


impl<'a> Password<'a> {
    // Create a new password object based on a line
    pub fn new(string: &'a String) -> Password<'a> {
        lazy_static! {
            static ref REGEX_LINE: Regex = Regex::new(r"(?P<min>\d+)-(?P<max>\d+) (?P<char>.): (?P<password>.+)").unwrap();
        }
        // Parse the line and build the object
        let captures = REGEX_LINE.captures(string).unwrap();
        Password {
            min: Password::parse_capture(&captures, "min").unwrap(),
            max: Password::parse_capture(&captures, "max").unwrap(),
            char: Password::parse_capture(&captures, "char").unwrap(),
            str: captures.name("password").unwrap().as_str(),
        }
    }

    fn parse_capture<T: FromStr>(captures: &Captures<'_>, name: &str) -> Result<T, T::Err> {
        captures.name(name).unwrap().as_str().parse()
    }
}
