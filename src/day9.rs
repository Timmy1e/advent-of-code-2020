use std::collections::VecDeque;

const PREAMBLE_SIZE: usize = 25;


///# Day 9: Encoding Error
/// https://adventofcode.com/2020/day/8
#[aoc_generator(day9)]
pub fn prepare_input(input: &str) -> Vec<usize> {
    input.lines().map(|s| s.trim().parse().unwrap()).collect()
}


///## What is the first number that does not have this property?
/// `36845998`
#[aoc(day9, part1)]
pub fn solve_part1(input: &Vec<usize>) -> Option<usize> {
    let mut queue = VecDeque::with_capacity(PREAMBLE_SIZE);
    let mut input_iter = input.iter();

    // Fill preamble
    for _ in 0..PREAMBLE_SIZE {
        queue.push_back(*input_iter.next().unwrap());
    }

    // Find first error
    for &current_value in input_iter {
        // Check if value is valid
        if !is_valid(&current_value, &queue) {
            return Some(current_value);
        }

        // Push current value into the queue, pop first so we don't exceed our capacity
        queue.pop_front();
        queue.push_back(current_value);
    }

    None
}

///## What is the encryption weakness in your XMAS-encrypted list of numbers?
/// `4830226`
#[aoc(day9, part2)]
pub fn solve_part2(input: &Vec<usize>) -> Option<usize> {
    let target_sum = solve_part1(input).expect("Could not solve part 1 with the input, part 2 is dependent on part 1");
    let mut weakness_queue = VecDeque::new();
    let mut current_sum = 0;

    for &current_value in input {
        // Our starting sum doesn't count
        if current_value == target_sum { continue; }

        // Push the current value into the queue
        weakness_queue.push_back(current_value);
        // Calculate the new sum
        current_sum += current_value;

        // While the sum is larger then the target
        while current_sum > target_sum {
            // Pop values from the queue and subtract them from the sum
            current_sum -= weakness_queue.pop_front().unwrap();
        }

        // Are we at the target_sum?
        if current_sum == target_sum {
            // Set the default values to the opposite extremes, so any value change it
            let mut weakness = (usize::max_value(), usize::min_value());

            // Min max each value in the queue to get the total min and max
            for value in weakness_queue {
                weakness.0 = weakness.0.min(value);
                weakness.1 = weakness.1.max(value);
            }

            // Return the sum of the min and max
            return Some(weakness.0 + weakness.1);
        }
    }

    None
}


fn is_valid(&target: &usize, queue: &VecDeque<usize>) -> bool {
    // For each value in the queue
    for &value in queue {
        // If it is larger then our target skip, as remainder is negative
        if value > target { continue; }
        let remainder = target - value;
        // If remainder isn't the value (no doubles) and contained in the queue
        if remainder != value && queue.contains(&remainder) {
            // We have a winner
            return true;
        }
    }
    // No sums found in the queue, so go home
    false
}
